﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.IdentityModel.Protocols;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;

[assembly: OwinStartup(typeof(SIPOpenIDClient.App_Start.Startup))]

namespace SIPOpenIDClient.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            JwtSecurityTokenHandler.InboundClaimTypeMap = new Dictionary<string, string>();
            System.Diagnostics.Debugger.Launch();
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "Cookies"
            });


            app.UseOpenIdConnectAuthentication(new OpenIdConnectAuthenticationOptions
            {
                ClientId = "YourClientId",
                Authority = "https://identity.siplay.com",
                ClientSecret = "YourClientSecret",
                ResponseType = "code id_token",
                Scope = "openid profile offline_access Authorization",
                PostLogoutRedirectUri = "http://localhost",
                TokenValidationParameters = new TokenValidationParameters
                {
                    NameClaimType = "name",
                    RoleClaimType = "role"
                },

                SignInAsAuthenticationType = "Cookies",
                Notifications = GetNotificationOptions(),
                UseTokenLifetime = false
            });
        }

        private OpenIdConnectAuthenticationNotifications GetNotificationOptions()
        {
            return new OpenIdConnectAuthenticationNotifications
            {
                AuthorizationCodeReceived =
                    async n =>
                    {
                        // use the code to get the access and refresh token
                        var tokenClient = new TokenClient(
                            new Uri(n.Options.Authority + "/connect/token")
                                .ToString(),
                            n.Options.ClientId,
                            n.Options.ClientSecret
                        );

                        var tokenResponse =
                            await tokenClient.RequestAuthorizationCodeAsync(
                                n.Code,
                                n.OwinContext.Request.Uri.ToString());

                        if (tokenResponse.IsError)
                        {
                            throw new Exception(tokenResponse.Error);
                        }

                        // use the access token to retrieve claims from userinfo
                        var userInfoClient =
                            new UserInfoClient(
                                new Uri(n.Options.Authority + "/connect/userinfo")
                                    .ToString());

                        var userInfoResponse =
                            await userInfoClient.GetAsync(tokenResponse.AccessToken);

                        // create new identity
                        var id = new ClaimsIdentity(
                            n.AuthenticationTicket.Identity.AuthenticationType);
                        id.AddClaims(userInfoResponse.Claims);
                        AddClaim(id, "access_token", tokenResponse.AccessToken);
                        AddClaim(id,
                            "expires_at",
                            DateTime.UtcNow.AddSeconds(tokenResponse.ExpiresIn)
                                .ToLocalTime().ToString());
                        AddClaim(id, "refresh_token", tokenResponse.RefreshToken);
                        AddClaim(id, "id_token", n.ProtocolMessage.IdToken);
                        AddClaim(id, "sid", n.AuthenticationTicket.Identity.FindFirst("sid").Value);

                        n.AuthenticationTicket = new AuthenticationTicket(
                            new ClaimsIdentity(
                                id.Claims,
                                n.AuthenticationTicket.Identity.AuthenticationType,
                                "name",
                                "role"),
                            n.AuthenticationTicket.Properties);
                    },
                RedirectToIdentityProvider = n =>
                {
                    n.ProtocolMessage.RedirectUri = n.Request.Uri.ToString();

                    // if signing out, add the id_token_hint
                    if (n.ProtocolMessage.RequestType == OpenIdConnectRequestType
                            .LogoutRequest)
                    {
                        // uncommenting this will auto logout of identity
                        /* var idTokenHint =
                             n.OwinContext.Authentication.User.FindFirst(
                                 "id_token" );

                         if ( idTokenHint != null )
                         {
                             n.ProtocolMessage.IdTokenHint = idTokenHint.Value;
                         }*/
                    }

                    return Task.FromResult(0);
                }
            };
        }

        private void AddClaim(ClaimsIdentity identity, string type, string value)
        {
            identity.AddClaim(new System.Security.Claims.Claim(type, value));
        }
    }
}
